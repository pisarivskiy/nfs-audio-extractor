#*.ABK and *.GIN extraction script - Need For Speed Underground (2) audio files extractor

##Dependencies

  * Node.js v8 and higher

##Extraction

Place your *.abk and *.gin files to the __source__ folder and run the script or run the following command in the console:

    npm start

After finishing the extraction, you can find *.wav files in the __dist__ folder
___
_.exe files are copyrighted by daemon1 (I think) and downloaded from [here](http://forum.xentax.com/viewtopic.php?f=17&t=13430)_